using System;
using System.Collections.Generic;
using System.Linq;
using Testy.DataAccess;
using Testy.Entities;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            EFUnitOfWork unit = new EFUnitOfWork(new QuizContext());

            foreach (var category in unit.Categories.Get().Where(cat => cat.ParentCategory == null))
            {
                Console.WriteLine("{0} has subs:", category.Name);

                foreach (var subCategory in category.SubCategories)
                {
                    Console.WriteLine("\t- {0}", subCategory.Name);
                }

                Console.WriteLine(new string('-', 15));
            }

            Console.ReadKey();
        }
    }
}
