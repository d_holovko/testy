﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public partial class GradeCategoryEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<GradeEntity> Grades { get; set; }

        public GradeCategoryEntity()
        {
            Name = default(string);

            Grades = new List<GradeEntity>();
        }
    }
}
