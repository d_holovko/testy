﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public partial class GradeEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public int Value { get; set; }
        public virtual GradeCategoryEntity Category { get; set; }
        
        public virtual ICollection<QuestionEntity> Questions { get; set; }

        public GradeEntity()
        {
            Value = default(int);
            Category = new GradeCategoryEntity();

            Questions = new List<QuestionEntity>();
        }
    }
}
