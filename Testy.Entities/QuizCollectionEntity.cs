﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public class QuizCollectionEntity: IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<QuizEntity> Quizzes { get; set; }

        public QuizCollectionEntity()
        {
            Quizzes = new List<QuizEntity>();
        }
    }
}
