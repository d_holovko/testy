﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public partial class TagEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<QuestionEntity> Questions { get; set; }

        public TagEntity()
        {
            Name = default(string);

            Questions = new List<QuestionEntity>();
        }
    }
}
