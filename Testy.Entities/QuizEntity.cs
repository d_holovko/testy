﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public class QuizEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<QuestionEntity> Questions { get; set; }

        public virtual ICollection<QuizCollectionEntity> QuizCollections { get; set; }

        public QuizEntity()
        {
            QuizCollections = new List<QuizCollectionEntity>();
        }
    }
}
