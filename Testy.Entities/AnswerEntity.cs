﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testy.Entities
{
    public partial class AnswerEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public bool IsCorrect { get; set; }
        public string Content { get; set; }

        public virtual QuestionEntity Question { get; set; }

        public AnswerEntity()
        {
            IsCorrect = default(bool);
            Question = new QuestionEntity();
        }
    }
}
