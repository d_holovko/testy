﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public partial class QuestionEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public virtual GradeEntity Grade { get; set; }
        public virtual ICollection<AnswerEntity> Answers { get; set; }
        public virtual ICollection<TagEntity> Tags { get; set; }
        public virtual ICollection<CategoryEntity> Categories { get; set; }
        public virtual ICollection<CommentaryEntity> Commentaries { get; set; }

        public virtual ICollection<QuizEntity> Quizzes { get; set; }

        public QuestionEntity()
        {
            Grade = new GradeEntity();
            Answers = new List<AnswerEntity>();
            Tags = new List<TagEntity>();
            Categories = new List<CategoryEntity>();
            Commentaries = new List<CommentaryEntity>();

            Quizzes = new List<QuizEntity>();
        }
    }
}
