﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public partial class CategoryEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<CategoryEntity> SubCategories { get; set; }

        public virtual CategoryEntity ParentCategory { get; set; }
        public virtual ICollection<QuestionEntity> Questions { get; set; }

        public CategoryEntity()
        {
            Name = default(string);
            ParentCategory = null;
            SubCategories = new List<CategoryEntity>(0);

            Questions = new List<QuestionEntity>();
        }
    }
}