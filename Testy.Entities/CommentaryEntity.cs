﻿using System;
using System.Collections.Generic;

namespace Testy.Entities
{
    public partial class CommentaryEntity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Content { get; set; }

        public virtual ICollection<QuestionEntity> Questions { get; set; }

        public CommentaryEntity()
        {
            Content = default(string);

            Questions = new List<QuestionEntity>();
        }
    }
}
