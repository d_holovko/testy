﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testy.Entities;

namespace Testy.DataAccess
{
    public abstract class EFRepository<TKey, TEntity> : IRepository<TKey, TEntity> where TEntity : class, IEntity<TKey>
    {
        private IDbContext context;
        private DbSet<TEntity> entities;

        public EFRepository(IDbContext context)
        {
            this.context = context;
            entities = context.Set<TEntity>();
        }

        public virtual void Create(TEntity item)
        {
            entities.Add(item);
        }

        public virtual TEntity Find(TKey id)
        {
            return entities.Find(id);
        }

        public virtual TEntity Find(TEntity item)
        {
            return Find(item);
        }

        public virtual IEnumerable<TEntity> Get()
        {
            return entities;
        }

        public virtual void Update(TEntity item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public virtual void Delete(TKey id)
        {
            var item = entities.Find(id);

            if (item == null) return;

            entities.Remove(item);
        }

        public virtual void Delete(TEntity item)
        {
            entities.Remove(item);
        }
    }
}
