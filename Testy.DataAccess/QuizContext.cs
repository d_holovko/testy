﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Testy.Entities;

namespace Testy.DataAccess
{
    public class QuizContext : DbContext, IDbContext
    {
        static QuizContext()
        {
            System.Data.Entity.Database.SetInitializer<QuizContext>(new QuizContextInitializer());
        }

        public QuizContext()
            : base("TestyDB")
        {}

        public new int SaveChanges()
        {
            return base.SaveChanges();
        }

        public new DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public new DbEntityEntry<TEntity> Entry<TEntity> (TEntity item) where TEntity: class
        {
            return base.Entry(item);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QuizCollectionEntity>().ToTable("QuizCollections")
                .HasMany(t => t.Quizzes)
                .WithMany(t => t.QuizCollections);

            modelBuilder.Entity<QuizEntity>().ToTable("Quizzes")
                .HasMany(q => q.Questions)
                .WithMany(q => q.Quizzes);

            modelBuilder.Entity<QuestionEntity>().ToTable("Questions")
                .HasOptional(q => q.Grade)
                .WithMany(q => q.Questions);

            modelBuilder.Entity<AnswerEntity>().ToTable("Answers")
                .HasRequired(a => a.Question)
                .WithMany(q => q.Answers);

            modelBuilder.Entity<TagEntity>().ToTable("Tags")
                .HasMany(t => t.Questions)
                .WithMany(q => q.Tags);

            modelBuilder.Entity<CategoryEntity>().ToTable("Categories")
                .HasMany(cat => cat.Questions)
                .WithMany(q => q.Categories);

            modelBuilder.Entity<CategoryEntity>()
                .HasMany(cat => cat.SubCategories)
                .WithOptional(cat => cat.ParentCategory);

            modelBuilder.Entity<CommentaryEntity>().ToTable("Commentaries")
                .HasMany(com => com.Questions)
                .WithMany(q => q.Commentaries);

            modelBuilder.Entity<GradeEntity>().ToTable("Grades")
                .HasOptional(g => g.Category)
                .WithMany(c => c.Grades);

            base.OnModelCreating(modelBuilder);
        }
    }
}
