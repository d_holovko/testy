﻿using System.Collections.Generic;
using Testy.Entities;

namespace Testy.DataAccess
{
    public interface IRepository<TKey, TEntity> where TEntity : class, IEntity<TKey>
    {
        void Create(TEntity item);
        TEntity Find(TKey id);
        TEntity Find(TEntity item);
        IEnumerable<TEntity> Get();
        void Update(TEntity item);
        void Delete(TKey id);
        void Delete(TEntity item);
    }
}
