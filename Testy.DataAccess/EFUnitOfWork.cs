﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testy.Entities;

namespace Testy.DataAccess
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private IDbContext context;
        private IRepository<Guid, CategoryEntity> categoryRepository;

        public EFUnitOfWork(IDbContext context)
        {
            this.context = context;
        }

        public IRepository<Guid, CategoryEntity> Categories
        {
            get
            {
                if (categoryRepository == null)
                {
                    categoryRepository = new EFCategoryRepository(context);
                }

                return categoryRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
