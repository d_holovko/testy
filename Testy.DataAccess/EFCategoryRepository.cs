﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testy.Entities;

namespace Testy.DataAccess
{
    public class EFCategoryRepository: EFRepository<Guid, CategoryEntity>
    {
        public EFCategoryRepository(IDbContext context) : base(context)
            {}


    }
}