namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsToQuestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("Questions", "Title", builder => builder.String(nullable: false));
            AddColumn("Questions", "Content", builder => builder.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Questions", "Title");
            DropColumn("Questions", "Content");
        }
    }
}
