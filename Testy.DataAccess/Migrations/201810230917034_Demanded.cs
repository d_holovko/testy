namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Demanded : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Categories", newName: "Categories");
            RenameColumn(table: "dbo.Categories", name: "ParentCategory_Id", newName: "ParentCategory_Id");
            RenameIndex(table: "dbo.Categories", name: "IX_ParentCategory_Id", newName: "IX_ParentCategory_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Categories", name: "IX_ParentCategory_Id", newName: "IX_ParentCategory_Id");
            RenameColumn(table: "dbo.Categories", name: "ParentCategory_Id", newName: "ParentCategory_Id");
            RenameTable(name: "dbo.Categories", newName: "Categories");
        }
    }
}
