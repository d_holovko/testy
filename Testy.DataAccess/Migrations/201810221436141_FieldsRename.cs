namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldsRename : DbMigration
    {
        public override void Up()
        {
            RenameColumn("Categories", "Category_Id", "ParentCategory_Id");
            RenameColumn("Tags", "Name", "Value");
            RenameColumn("Quizzes", "Name", "Title");
            RenameColumn("QuizCollections", "Name", "Title");
        }
        
        public override void Down()
        {
            RenameColumn("Categories", "ParentCategory_Id", "Category_Id");
            RenameColumn("Tags", "Value", "Name");
            RenameColumn("Quizzes", "Title", "Name");
            RenameColumn("QuizCollections", "Title", "Name");
        }
    }
}
