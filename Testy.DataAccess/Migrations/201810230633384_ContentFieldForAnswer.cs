namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContentFieldForAnswer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "Content", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "Content");
        }
    }
}
