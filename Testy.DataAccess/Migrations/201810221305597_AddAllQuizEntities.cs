namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAllQuizEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Grade_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Grades", t => t.Grade_Id)
                .Index(t => t.Grade_Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Category_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.Commentaries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Value = c.Int(nullable: false),
                        Category_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GradeCategories", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.GradeCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CategoriesQuestions",
                c => new
                    {
                        Category_Id = c.Guid(nullable: false),
                        Question_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new {t.Category_Id, t.Question_Id })
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_Id, cascadeDelete: true)
                .Index(t => t.Category_Id)
                .Index(t => t.Question_Id);
            
            CreateTable(
                "dbo.CommentariesQuestions",
                c => new
                    {
                        Commentary_Id = c.Guid(nullable: false),
                        Question_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new {t.Commentary_Id, t.Question_Id })
                .ForeignKey("dbo.Commentaries", t => t.Commentary_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_Id, cascadeDelete: true)
                .Index(t => t.Commentary_Id)
                .Index(t => t.Question_Id);
            
            CreateTable(
                "dbo.QuestionsQuizzes",
                c => new
                    {
                        Question_Id = c.Guid(nullable: false),
                        Quiz_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new {t.Question_Id, t.Quiz_Id })
                .Index(t => t.Question_Id)
                .Index(t => t.Quiz_Id);

            AddForeignKey("dbo.QuestionsQuizzes", "Question_Id", "dbo.Questions", "Id", true);
            AddForeignKey("dbo.QuestionsQuizzes", "Quiz_Id", "dbo.Quizzes", "Id", true);

            CreateTable(
                "dbo.TagsQuestions",
                c => new
                    {
                        Tag_Id = c.Guid(nullable: false),
                        Question_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new {t.Tag_Id, t.Question_Id })
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_Id, cascadeDelete: true)
                .Index(t => t.Tag_Id)
                .Index(t => t.Question_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagsQuestions", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.TagsQuestions", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.QuestionsQuizzes", "Quiz_Id", "dbo.Quizzes");
            DropForeignKey("dbo.QuestionsQuizzes", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.Questions", "Grade_Id", "dbo.Grades");
            DropForeignKey("dbo.Grades", "Category_Id", "dbo.GradeCategories");
            DropForeignKey("dbo.CommentariesQuestions", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.CommentariesQuestions", "Commentaries_Id", "dbo.Commentaries");
            DropForeignKey("dbo.Categories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.CategoriesQuestions", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.CategoriesQuestions", "Category_Id", "dbo.Categories");
            DropIndex("dbo.TagsQuestions", new[] { "Question_Id" });
            DropIndex("dbo.TagsQuestions", new[] { "Tag_Id" });
            DropIndex("dbo.QuestionsQuizzes", new[] { "Quiz_Id" });
            DropIndex("dbo.QuestionsQuizzes", new[] { "Question_Id" });
            DropIndex("dbo.CommentariesQuestions", new[] { "Question_Id" });
            DropIndex("dbo.CommentariesQuestions", new[] { "Commentary_Id" });
            DropIndex("dbo.CategoriesQuestions", new[] { "Question_Id" });
            DropIndex("dbo.CategoriesQuestions", new[] { "Category_Id" });
            DropIndex("dbo.Grades", new[] { "Category_Id" });
            DropIndex("dbo.Categories", new[] { "Category_Id" });
            DropIndex("dbo.Questions", new[] { "Grade_Id" });
            DropTable("dbo.TagsQuestions");
            DropTable("dbo.QuestionsQuizzes");
            DropTable("dbo.CommentariesQuestions");
            DropTable("dbo.CategoriesQuestions");
            DropTable("dbo.Tags");
            DropTable("dbo.GradeCategories");
            DropTable("dbo.Grades");
            DropTable("dbo.Commentaries");
            DropTable("dbo.Categories");
            DropTable("dbo.Questions");
        }
    }
}
