namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnswersForQuestions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsCorrect = c.Boolean(nullable: false),
                        Question_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Question_Id);
            
            AddForeignKey("dbo.Answers", "Question_Id", "Questions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Answers", "Question_Id", "dbo.Questions");
            DropIndex("dbo.Answers", new[] { "Question_Id" });
            DropTable("dbo.Answers");
        }
    }
}
