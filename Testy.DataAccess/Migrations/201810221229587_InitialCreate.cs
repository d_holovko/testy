namespace Testy.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuizCollections",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Quizzes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuizCollectionQuizzes",
                c => new
                    {
                        QuizCollection_Id = c.Guid(nullable: false),
                        Quiz_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuizCollection_Id, t.Quiz_Id })
                .ForeignKey("dbo.QuizCollections", t => t.QuizCollection_Id, cascadeDelete: true)
                .ForeignKey("dbo.Quizzes", t => t.Quiz_Id, cascadeDelete: true)
                .Index(t => t.QuizCollection_Id)
                .Index(t => t.Quiz_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuizCollectionQuizzes", "Quiz_Id", "dbo.Quizzes");
            DropForeignKey("dbo.QuizCollectionQuizzes", "QuizCollection_Id", "dbo.QuizCollections");
            DropIndex("dbo.QuizCollectionQuizzes", new[] { "Quiz_Id" });
            DropIndex("dbo.QuizCollectionQuizzes", new[] { "QuizCollection_Id" });
            DropTable("dbo.QuizCollectionQuizzes");
            DropTable("dbo.Quizzes");
            DropTable("dbo.QuizCollections");
        }
    }
}
