﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using Testy.Entities;

namespace Testy.DataAccess
{
    public class QuizContextInitializer: DropCreateDatabaseAlways<QuizContext>
    {
        protected override void Seed(QuizContext context)
        {
            var fakeCategories = new Faker<CategoryEntity>()
                .RuleFor(cat => cat.Id, Guid.NewGuid)
                .RuleFor(cat => cat.Name, f => f.Lorem.Word());

            var fakeRootCategories = new Faker<CategoryEntity>()
                .RuleFor(cat => cat.Id, Guid.NewGuid)
                .RuleFor(cat => cat.Name, f => f.Lorem.Word())
                .RuleFor(cat => cat.SubCategories, f => fakeCategories.Generate(f.Random.Int(0, 2)).ToList());

            var categories = fakeRootCategories.Generate(10);

            /*categories[2].SubCategories.Add(categories[3]);
            categories[3].ParentCategory = categories[2];
            categories[2].SubCategories.Add(categories[4]);
            categories[4].ParentCategory = categories[2];*/

            context.Set<CategoryEntity>().AddRange(categories);

            context.SaveChanges();
        }
    }
}
