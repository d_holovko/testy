﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Testy.Web.Startup))]
namespace Testy.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
